import os
from conans import ConanFile, AutoToolsBuildEnvironment, tools

class Base(object):
    author = "Florian Schmidt florian.schmidt@dlr.de"
    url = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"

    prefix = "/conan-prefix"
    install_sandbox = "install"
    python_path = "lib/python/site-packages"

    options = {
    }
    default_options = {
        "OpenSSL:shared": False,
    }
    
    def set_attrs(self, derived):
        """
        to be called in derived conanfile's init() method via self.python_requires["ln_conan"].module.Base.set_attr(self)
        """
        for attr in "author,url,prefix,install_standbox,python_path".split(","):
            setattr(derived, attr, getattr(self, attr))

    def build_requirements(self):
        self.build_requires("scons-local/3.1.1@3rdparty/stable")

    def same_branch_or(self, req):
        """
        if self.version looks like a branch, return that.
        if not, return req
        """
        if self.version[:1].isdigit() and "." in self.version:
            pass # looks like a version, use req
        else:
            req = self.version
        return "%s@%s/%s" % (req, self.user, self.channel)

    def write_version_header(self, define, version_file):
        """
        write a file that can be included by a c-compiler.
        `define` - the name if the version-define to use
        """
        with open(version_file, "wb") as fp:
            fp.write(('#define %s "%s"\n' % (define, self.version)).encode("utf-8"))

    def write_version_file(self, version_file):
        """
        write a plain file with just the package's version
        """
        dn = os.path.dirname(version_file)
        if dn and not os.path.isdir(dn):
            os.makedirs(dn)
        with open(version_file, "wb") as fp:
            fp.write(("%s" % self.version).encode("utf-8"))

    def get_vars(self):
        """
        return build environment variables from AutoToolsBuildEnvironment
        """
        env = AutoToolsBuildEnvironment(self)
        evars = env.vars
        replaces = [
            "-O3 -s"
        ]
        self.output.info("auto tools build environment variables:")
        for key in sorted(evars.keys()):
            value = evars[key]
            self.output.info("  %s = %r" % (key, value))
            had_replace = False
            for rep in replaces:
                if rep in value:
                    had_replace = True
                    value = value.replace(rep, "")
            if had_replace:
                evars[key] = value
        return evars

    def scons_build(self, subdirs=None, opts="", add_flags=None, verbose=True, with_python_lib=False, parallel=True):
        bt = self.settings.get_safe("build_type")
        if bt and "debug" in bt.lower():
            debug_opt = '--without-opt'
        else:
            #debug_opt = '--without-debug'
            debug_opt = ""

        if subdirs:
            subdirs = '--subdirs="%s"' % subdirs
        else:
            subdirs = ""

        if parallel:
            if verbose:
                debug_opt += " V=parallel"
            debug_opt += " --jobs=%s" % tools.cpu_count()
        else:
            debug_opt += " V=1" # do always verbose build, which is not parallel

        if "python_version" in self.options:
            with_python = "python%s" % self.options.python_version
        else:
            with_python = False

        vars = self.get_vars()
        if add_flags:
            for key, value in add_flags.items():
                vars[key] = vars.get(key, "") + " " + value
        with tools.environment_append(vars):
            cmd = 'scons %s --from-env %s %s "--prefix=%s" "--install-sandbox=%s"' % (
                subdirs, debug_opt, opts, self.prefix, self.install_sandbox)
            if with_python:
                cmd += ' "--python=%s"' % with_python
            if with_python_lib:
                cmd += ' "--python-lib=%s"' % (self.python_path)
                
            self.output.info("run: %s" % cmd)
            self.run(cmd)
            
class ln_conan(ConanFile):
    name = "ln_conan"
    author = "Florian Schmidt florian.schmidt@dlr.de"
    url = "https://rmc-github.robotic.dlr.de/common/links_and_nodes"
